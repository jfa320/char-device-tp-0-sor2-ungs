# TP0 - SOR2: Device Drivers 

## Descripción 
Se deberá realizar un módulo del kernel para un char device en linux. El char device deberá leer un mensaje y mostrarlo al revés.

## Instalación del Driver

1. Primero se deberá descargar el repositorio. Para ello ejecutar desde una terminal (previamente debe tener instalado Git):

```
git clone https://gitlab.com/jfa320/char-device-tp-0-sor2-ungs
```

2. Ejecutar un make clean desde la terminal para limpiar el directorio (parado en el directorio donde se bajó el repositorio):

```
make clean
```

3. Ejecutar make:

```
make
```

4. Agregar el módulo al kernel con: 

```
sudo insmod juarezDriver.ko
```

Podemos verificar su correcta instalación viendo los logs del kernel con el comando:

```
dmesg
```
![Driver registrado.jpg](img/Driver registrado.jpg)

5. (OPCIONAL) Si hubo una instalación anterior, se deberá eliminar el directorio donde estaba el driver anterior para evitar algún inconveniente:
```
sudo rm /dev/juarezDriver
```

6. Ejecutar los siguientes comandos para inicializar el driver:

```
sudo mknod /dev/juarezDriver c 240 0
sudo chmod 666 /dev/juarezDriver
```


## Ejecución del Driver

1. Ejecutar un echo con algún mensaje cualquiera al driver:

```
echo 'Hola' > /dev/juarezDriver
```

2. Levantar el mensaje del driver con cat:

```
cat /dev/juarezDriver
```
Finalmente veo el resultado:

![Resultado Ejecución.jpg](img/Resultado Ejecución.jpg)

## Autor

* **Fabián Juárez** - [jfa320](https://github.com/jfa320)

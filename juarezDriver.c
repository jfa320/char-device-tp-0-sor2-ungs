/*
Se utiliza como guia el chardev.c de The Linux Kernel Module Programming
*/
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h> 

/*
Prototipos
*/

int init_module(void); //para iniciar el driver - al ejecutar insmod
void cleanup_module(void); //para finalizar el driver - al ejecutar rmmod
static int device_open(struct inode *, struct file *); // llamada cuando un proceso intenta abrir el archivo asociado al driver
static int device_release(struct inode *, struct file *); // llamada cuando un proceso intenta abrir el archivo asociado al driver
static ssize_t device_read(struct file *, char *, size_t, loff_t *);// cuando un proceso lee el dispositivo
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);// cuando un proceso escribe el dispositivo

#define SUCCESS 0
#define DEVICE_NAME "juarezDriver"   /* nombre del dispositivo  */
#define BUF_LEN 150              /* buffer con el tamaño maximo del mensaje que se le pasa*/

/*  Variables globales */
static int Major;               /* Major number  */
static int Device_Open = 0;     /* Para saber si el dispositivo esta abierto. Es un flag para evitar multiples apertura del driver */
static char msg[BUF_LEN];       /* Mensaje a mostrar */
static char *msg_Ptr;           /*puntero al mensaje */




//creo struct con operaciones que necesito para el driver
struct file_operations fops = {        
  .read = device_read,        
  .write = device_write,        
  .open = device_open,        
  .release = device_release
};

int init_module(void){ 
  /* Constructor */
  Major = register_chrdev(0, DEVICE_NAME, &fops); //registro el driver en el kernel. Le paso cero para que me genere un major,
                                                  //el nombre del dispositivo y las operaciones que requiero

  if (Major < 0) {      // validacion por si surge un error al registrar el driver    
    printk(KERN_ALERT "Registro del char device falló con major: %d\n", Major);          
    return Major;        
  }

  printk(KERN_INFO "%s: Driver registrado\n",DEVICE_NAME);
  printk(KERN_INFO "%s: Si hubo una ejecución anterior, borrar device file.\n",DEVICE_NAME);
  printk(KERN_INFO "%s: Para borrar \n",DEVICE_NAME);
  printk(KERN_INFO "%s: 'sudo rm /dev/%s'.\n", DEVICE_NAME, DEVICE_NAME);     
  printk(KERN_INFO "%s: Me asignaron major number %d. Para enviar mensaje\n", DEVICE_NAME,Major);
  printk(KERN_INFO "%s: al driver, crear un archivo dev con \n",DEVICE_NAME);
  printk(KERN_INFO "%s: 'sudo mknod /dev/%s c %d 0'.\n", DEVICE_NAME,DEVICE_NAME,Major);    
  printk(KERN_INFO "%s: 'sudo chmod 666 /dev/%s'.\n", DEVICE_NAME, DEVICE_NAME);      
  printk(KERN_INFO "%s: Probar hacer cat (para levantar mensaje enviado) o echo (para enviar mensaje) al\n", DEVICE_NAME);        
  printk(KERN_INFO "%s: device file.\n", DEVICE_NAME);        
  printk(KERN_INFO "%s: Borrar device file (ver arriba) y módulo del driver ('sudo rmmod %s') cuando termines.\n",DEVICE_NAME, DEVICE_NAME);   
  return SUCCESS;
}

void cleanup_module(void){ 
  /* Destructor */
  unregister_chrdev(Major, DEVICE_NAME); //desregistro el device driver             
  printk(KERN_INFO "%s: Driver desregistrado \n",DEVICE_NAME);
  printk(KERN_INFO "-----------------------------------------------------------------------------------------\n");  
}

//llamado cuando un proceso intenta abrir el driver 
static int device_open(struct inode *inode, struct file *file){
            
  //validacion para evitar multiples aperturas del dispositivo     
  if (Device_Open){
    return -EBUSY;
  }                
                
  Device_Open++; //cambio el flag para avisar que el dispositivo esta en uso 


  msg_Ptr = msg;    
  printk(KERN_INFO "%s: El mensaje escrito fue: %s \n",DEVICE_NAME,msg_Ptr);  
  try_module_get(THIS_MODULE);   //incremento el contador de uso del modulo     
  return SUCCESS;
}

//llamado cuando cierro el driver 
static int device_release(struct inode *inode, struct file *file){
  Device_Open--;          //decremento el flag para poder brindar acceso al driver a otro proceso   
  module_put(THIS_MODULE);    //decremento el contador de uso del modulo    
  return 0;
}

//llamada cuando un proceso que ya abrio el driver, intenta leer de el -> ej: al usar cat /dev/juarezDriver
static ssize_t device_read(struct file *filp,   char *buffer, size_t length, loff_t * offset){        
  //el buffer es donde voy a poner en el espacio de usuario el texto
  //length es el largo del buffer
  int bytes_read = 0; //bytes leidos
  //validacion para ver si estoy en el final del texto      
  if (*msg_Ptr == 0) {
    return 0;
  }               
              
  //pongo el texto en el buffer      
  while (length && *msg_Ptr) {                
    put_user(*(msg_Ptr++), buffer++); //pongo en el buffer (user space) el contenido de la direccion actual del puntero al mensaje              
    length--;                
    bytes_read++;        
  }   

//devuelvo los bytes leidos (por convención)                 
return bytes_read;

}


//llamada cuando un proceso escribe en el driver file -> ej: echo "Hola" > /dev/juarezDriver
static ssize_t device_write(struct file *filp, const char *buffer, size_t length, loff_t * offset){        
  int i;
  for (i = 0; i < length && i < BUF_LEN; i++){
      get_user(msg[i], buffer + (length - i -2)); //obtengo lo que me pasa el usuario y lo coloco en
                                                  //la variable msg de forma invertida
                                                  //length - i -> es para empezar de atras para adelante
                                                  // -2 -> es para saltearme el salto de linea que tiene cada string
  }

  //Agrego salto de linea al string
  get_user(msg[i-1], buffer + (length-1) ); // en length-1 se da el salto de linea (en consola), por eso debo agregarselo al final,
                                            //luego de invertir el string pasado
  
  buffer=""; //vacio el buffer para evitar errores en futuras escrituras 
  msg_Ptr = msg; 
  printk(KERN_INFO "%s: El mensaje escrito fue: %s \n",DEVICE_NAME,msg_Ptr);  
  return i;
}


MODULE_LICENSE("GPL");
MODULE_AUTHOR("Juárez Fabián");
MODULE_DESCRIPTION("Un primer driver");
